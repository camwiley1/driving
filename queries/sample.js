db.rankings.aggregate(
    [
        {$match: {nombre_autoescuela: "roda"}},
        {
            $group: {
                _id       : "$codigo_autoescuela",
                conv_1    : {$sum: "$num_aptos_1conv"},
                conv_2    : {$sum: "$num_aptos_2conv"},
                conv_3o4  : {$sum: "$num_aptos_3o4conv"},
                conv_5omas: {$sum: "$num_aptos_5_o_mas_conv"},
                passed    : {$sum: {$add: ["$num_aptos_1conv", "$num_aptos_2conv"]}},
                failed    : {$sum: {$add: ["$num_aptos_3o4conv", "$num_aptos_5_o_mas_conv"]}},
                num_aptos : {$sum: "$num_aptos"}
            }
        }
    ]
);
