var express = require('express'),
    //i18n = require('i18n'),
    //i18n = new (require('i18n-2'))({
    //    // setup some locales - other locales default to the first locale
    //    locales: ['de']
    //}),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    app = express(),
    http = require('http'),
    parse = require('csv-parse'),
    routes = {
        //posts : require('./controllers/api/posts'),
        static: require('./controllers/static'),
        api: require('./controllers/api/main')
    };

//i18n.setLocale('de');
//console.log(i18n.__("Hello"));

//i18n.expressBind(app, {
//    locales   : ['en', 'de'],
//    cookieName: 'locale'
//});
app.use(cookieParser());
//app.use(function (req, res, next) {
//    console.log(req.cookies);
////    req.i18n.setLocaleFromCookie();
//    next();
//});
app.use(bodyParser.json());
app.use('/api', routes.api);
app.use('/', routes.static);

app.listen(3000, function () {
    console.log('listening on', 3000)
});


