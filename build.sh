#!/usr/bin/env bash

program_exists()
{
    if hash $1 2>/dev/null; then
        echo true
    else
        echo false
    fi
}

pm2_exists=$(program_exists pm2)
if [[ $pm2_exists = true ]]; then
    pm2 gracefulReload all
    echo 'pm2 gracefulReload all'
else
    echo 'pm2 not found'
fi