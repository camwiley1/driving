var gulp = require('gulp'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    config = {
        modulePath: './ng',
        bowerDir: './bower_components',
        public: {
            js: 'public/js',
            css: 'public/css'
        }

    };

gulp.task('sass', function () {
    // gulp.src locates the source files for the process.
    // This globbing function tells gulp to use all files
    // ending with .scss or .sass within the scss folder.
    gulp.src('sass/**/*.{scss,sass}')
        // Converts Sass into CSS with Gulp Sass
        // Init sourcemaps
        .pipe(sourcemaps.init())
        .pipe(sass({
            errLogToConsole: true
        }))
        // Writes sourcemaps into the CSS file
        .pipe(sourcemaps.write())
        // Outputs CSS files in the css folder
        .pipe(concat('main.css'))
        .pipe(gulp.dest(config.public.css));
});

gulp.task('css', function () {
    gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'css/*'
    ])
        .pipe(concat('main.css'))
        .pipe(gulp.dest(config.public.css));
});

gulp.task('js-front', function () {
    gulp.src([
        config.bowerDir + '/angular/angular.js',
        config.bowerDir + '/angular-route/angular-route.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('front.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.public.js));

});

gulp.task('js-assets', function () {
    gulp.src([
        'ng/module.js',
        'ng/**/*.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.public.js));
});

gulp.task('build', ['js-front', 'js-assets', 'sass']);
gulp.task('watch:js', function () {
    gulp.watch('ng/**/*.js', ['js-assets']);

    gulp.watch(config.bowerDir + '/**/*.js', ['js-front']);
});
gulp.task('watch:sass', function () {
    gulp.watch('./sass/**/*.{scss,sass}', ['sass'])

});
gulp.task('watch:css', ['css'], function () {
    gulp.watch('css/*', ['css']);
});
gulp.task('watch', ['watch:js', 'watch:sass']);

gulp.task('default', ['build', 'watch']);
