var saveCentro = require('../npm/save-centro'),
    db = require('../db'),
    CentroExamen = require('../models/centro-examen');

var stream = CentroExamen.find().stream();

var count = 0;
var filteredDoc,
    prevName,
    newName;
stream.on('data', function (doc) {
    if (count % 10 === 0) {
        console.log('total:' + count);
    }

    prevName = doc.nombre;
    filteredDoc = saveCentro.filter(doc);
    if (filteredDoc.nombre !== prevName) {
        console.log(filteredDoc);
        console.log(prevName, filteredDoc.nombre);
    }
    filteredDoc.save(function (err) {
        if (err) {
            console.log(err);
        }
    });
    count++;

}).on('error', function (err) {
    // handle the error
}).on('close', function () {
    // the stream is closed
    console.log('total count:' + count);
    console.log('done');
});




