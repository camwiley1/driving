var saveCentro = require('../npm/save-centro'),
    db = require('../db'),
    Apto = require('../models/apto'),
    Promise = require('promise'),
    _ = require('lodash');

Apto.count(function (err, count) {
    if (err) {
        console.log('error');
        console.log(err);
        process.exit(1);
    }

    var totalCount = count;
    var decrement = 1000;
    saveCentros(totalCount, totalCount, decrement);
    console.log('There are %d kittens', count);
    //_.forEach(results, function (n, key) {
    //    updateData(n);
    //});
});

function reflect(promise) {
    return promise.then(function (v) {
            return {v: v, status: "resolved"}
        },
        function (e) {
            return {e: e, status: "rejected"}
        });
}

function saveCentros(totalCount, currentCount, decrement) {

    var completed = totalCount - currentCount;
    Apto.find().limit(decrement).skip(completed).exec(function (err, results) {
        var data;
        var promises = [];
        _.forEach(results, function (n, key) {
            data = {nombre: n.centro_examen, provincia: n.desc_provincia};
            var test = saveCentro.add(data);
            promises.push(test);
        });

        Promise.all(promises.map(reflect)).then(function (res) {
            currentCount -= decrement;
            console.log(completed);
            completed += decrement;
            if (completed >= totalCount) {
                console.log('done!');
                process.exit(0);
            }
            if (currentCount > 0) {
                saveCentros(totalCount, currentCount, decrement);
            }
        });
    });
}

