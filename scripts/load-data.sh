#!/usr/bin/env bash

MONTHS=(
    'ENERO.txt'
    'FEBRERO.txt'
    'MARZO.txt'
    'ABRIL.txt'
    'MAYO.txt'
    'JUNIO.txt'
    'JULIO.txt'
)

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DATA_DIR="../files/utf8"
NPM_DIR="../npm"

for MONTH in ${MONTHS[*]}
do
    node "$DIR/$NPM_DIR/save-data.js" "$DIR/$DATA_DIR/$MONTH"
done

node "$DIR/fix-aptos-nombre.js"

