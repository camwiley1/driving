var saveApto = require('../npm/save-apto'),
    db = require('../db'),
    _ = require('lodash'),
    q = require('q'),
    Autoescuela = require('../models/autoescuela'),
    Apto = require('../models/apto');

var stream = Apto.where('nombre_autoescuela').exists(false).stream();

var count = 0;
var updateCount = 0;
var noCodigoList = [];
var noNombreList = [];
var invalidList = [];
var foundList = [];
var promise = [];
stream.on('data', function (doc) {
    if (count % 10 === 0) {
        console.log('processed: ' + count);
    }

    invalidList.push(doc.codigo_autoescuela);

    promise.push(Autoescuela.findOne({codigo: doc.codigo_autoescuela}, function (err, autoescuela) {
        if (err) {
            console.log(err);
            return next(err);
        }

        if (!autoescuela) {
            noCodigoList.push(doc.codigo_autoescuela);
            return;
        }

        foundList.push(doc.codigo_autoescuela);

        doc.nombre_autoescuela = autoescuela.nombre;
        doc.save();
    }));
    count++;

}).on('error', function (err) {
    console.log(err);
    // handle the error
}).on('close', function () {
    // the stream is closed
    console.log('done!');
    q.all(promise).done(function (values) {
        console.log('promise all!');
        var noCode = _.unique(noCodigoList);
        console.log('no code: ' + noCode.length);
        console.log('no code list:', noCode);
        process.exit(0);
        //console.log(values);
    })
});




