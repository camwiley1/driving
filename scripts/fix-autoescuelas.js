var saveAutoescula = require('../npm/save-autoescuela');
    Apto = require('../models/apto');

var stream = Apto.find().stream();

var count = 0;
stream.on('data', function (doc) {
    count++;
    if (count % 1000 === 0) {
        console.log(count);
    }

    if (doc.nombre_autoescuela) {
        saveAutoescula.add({
            nombre: doc.nombre_autoescuela,
            codigo: doc.codigo_autoescuela
        });
    }
}).on('error', function (err) {
    console.log(err);
    // handle the error
}).on('close', function () {
    // the stream is closed
    console.log('total count:' + count);
});




