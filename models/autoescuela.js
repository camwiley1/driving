var db = require('../db');
var autoescuelaSchema = new db.Schema({
    codigo: {
        unique  : true,
        type    : String,
        lowercase: true,
        required: true
    },
    nombre: {
        type   : String,
        lowercase: true,
        require: true
    }
});

var Autoescuela = db.model('Autoescuela', autoescuelaSchema);
module.exports = Autoescuela;
