var db = require('../db');
var centroExamen = new db.Schema({
    nombre   : {
        type     : String,
        lowercase: true,
        required : true
    },
    provincia: {
        type     : String,
        lowercase: true,
        require  : true
    }
})
    .index({
        'nombre' : 1,
        'provincia': 1
    },
    {
        unique: true
    });

var CentroExamen = db.model('CentroExamen', centroExamen);
module.exports = CentroExamen;
