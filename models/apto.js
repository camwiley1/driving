var db = require('../db');
var aptoSchema = new db.Schema({
    uid: {
        unique: true,
        index: true,
        type: String,
        lowercase: true,
        required: true
    },
    desc_provincia        : {
        type    : String,
        lowercase: true,
        required: true
    },
    centro_examen         : {
        type    : String,
        lowercase: true,
        required: true
    },
    codigo_autoescuela    : {
        type    : String,
        lowercase: true,
        required: true
    },
    nombre_autoescuela    : {
        type   : String,
        lowercase: true,
        require: false
    },
    codigo_seccion        : {
        type   : String,
        lowercase: true,
        require: true
    },
    date : {
        type: Date,
        require: true
    },
    mes                   : {
        type   : Number,
        require: true
    },
    anyo                  : {
        type   : Number,
        require: true
    },
    tipo_examen           : {
        type   : String,
        lowercase: true,
        require: true
    },
    nombre_permiso        : {
        type   : String,
        lowercase: true,
        require: true
    },
    num_aptos             : {
        type   : Number,
        require: true
    },
    num_aptos_1conv       : {
        type   : Number,
        require: true
    },
    num_aptos_2conv       : {
        type   : Number,
        require: true
    },
    num_aptos_3o4conv     : {
        type   : Number,
        require: true
    },
    num_aptos_5_o_mas_conv: {
        type   : Number,
        require: true
    },
    num_no_aptos          : {
        type   : Number,
        require: true
    }
});

var Apto = db.model('Apto', aptoSchema);
module.exports = Apto;
