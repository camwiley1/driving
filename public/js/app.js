
angular.module('drivingApp', [
    'ngRoute'
])
    .constant('_', window._)
    .run(["$rootScope", "$timeout", function ($rootScope, $timeout) {
        $rootScope._ = window._;
        $rootScope.$on('$viewContentLoaded', ()=> {
            $timeout(()=> {
                componentHandler.upgradeAllRegistered();
        });
    })

    }]);

angular.module('drivingApp')
    .config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'SearchCtrl',
                templateUrl: 'search.html'
            })
            .when('/search', {
                controller: 'SearchCtrl',
                templateUrl: 'search.html'
            })
            .when('/search.all', {
                controller: 'AptosCtrl',
                templateUrl: 'search-all.html'
            })
            .when('/search.rankings', {
                controller : 'AptosSumCtrl',
                templateUrl: 'aptos-sum.html'
            })
            .when('/search.centros', {
                controller : 'CentrosCtrl',
                templateUrl: 'centros.html'
            })
            .when('/search.provincias', {
                controller : 'CentrosCtrl',
                templateUrl: 'provincias.html'
            })
            .when('/escuelas', {
                controller : 'EscuelasCtrl',
                templateUrl: 'escuelas.html'
            })
            .when('/rankings', {
                controller : 'RankingsCtrl',
                templateUrl: 'rankings.html'
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);


angular.module('drivingApp').controller('AptosSumCtrl', ["$scope", "myUtils", "AptosSrv", "_", function ($scope, myUtils, AptosSrv, _) {
    AptosSrv.fetchSum().then(function (posts) {
        var rawPosts = myUtils.promiseDataField(posts, 'aptos');
        var count = 1;
        var preppedPosts = _.map(rawPosts, function (n) {
            n.rank = count;
            n.date = new Date(n.anyo, n.mes, 1);
            count++;
            return n;
        });
        $scope.aptos = preppedPosts;
    });
}]);


angular.module('drivingApp').controller('AptosCtrl', ["$scope", "myUtils", "AptosSrv", "_", function ($scope, myUtils, AptosSrv, _) {
    AptosSrv.fetch().then(function (posts) {
        var rawPosts = myUtils.promiseDataField(posts, 'aptos');
        var preppedPosts = _.map(rawPosts, function (n) {
            n.date = new Date(n.anyo, n.mes, 1);
            return n;
        });
        $scope.aptos = preppedPosts;
    });
}]);


angular.module('drivingApp').controller('CentrosCtrl', ["$scope", "myUtils", "CentrosSvc", "_", function ($scope, myUtils, CentrosSvc, _) {
    CentrosSvc.fetchAll().then(function (results) {
        var centros = myUtils.promiseDataField(results, 'centros');
        $scope.centros = centros;
        $scope.provincias = _.unique(_.pluck(centros, 'provincia').sort());
    });
}]);


angular.module('drivingApp').controller('EscuelasCtrl', ["$scope", "myUtils", "EscuelasSrv", function ($scope, myUtils, EscuelasSrv) {
    EscuelasSrv.fetch().then(function (results) {
        $scope.escuelas = myUtils.promiseDataField(results, 'escuelas');
        $scope.count = myUtils.promiseDataField(results, 'count');
    });
}]);

angular.module('drivingApp').controller('RankingsCtrl', ["$scope", "myUtils", "AptosSrv", "_", "$routeParams", "$route", function ($scope, myUtils, AptosSrv, _, $routeParams, $route) {
    var opts = {
        min_aptos     : 20, // minimum number of applications
        limit         : 100,
        mes_b         : 7,
        anyo          : 2015,
        tipo_examen   : 'Conducción Y Circulación',
        nombre_permiso: 'b' // max number of results
    };

    var translateParams = {
        mes_a: 'De Mes',
        mes_b: 'A Mes',
        min_aptos: 'Minimo Aptos',
        centro_examen: 'Centro de Examen',
        limit: 'Limite de Resultados',
        anyo: 'Año',
        provincia: 'Provincia',
        tipo_examen: 'Tipo Examen',
        nombre_permiso: 'Nombre Permiso'
    };

    function rankingsSuccess(posts) {
        var rawPosts = myUtils.promiseDataField(posts, 'aptos');
        var count = 1;
        var preppedPosts = _.map(rawPosts, function (n) {
            n.rank = count;
            n.date = new Date(n.anyo, n.mes, 1);
            count++;
            return n;
        });
        $scope.aptos = preppedPosts;
        $scope.params = myUtils.promiseDataField(posts, 'params');

        var translatedParams = {};
        _.forEach($scope.params, function (n, key) {
            translatedParams[translateParams[key] || key] = n;
        });
        $scope.niceParams = translatedParams;
    }

    $scope.toggleFilterInputs = function () {
        $scope.showFilterInputs = ($scope.showFilterInputs) ? false : true;
    };

    var data = _.extend({}, $routeParams, opts);
    AptosSrv.fetchRankings(opts).then(rankingsSuccess);
    $scope.data = data;

    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.submitSearch = function () {
        AptosSrv.fetchRankings($scope.data).then(rankingsSuccess);
        $scope.toggleFilterInputs();
    };
}]);

angular.module('drivingApp').controller('SearchCtrl', ["$scope", "$location", "myUtils", "CentrosSvc", "_", function ($scope, $location, myUtils, CentrosSvc, _) {

    const PROVINCIA = 'provincia';
    const CENTRO = 'centro_examen';

    var my = {},
        cache = {};

    my.validateSelected = function (name) {
        return !!($scope.data && $scope.data[name])
    };

    my.validateAllSelected = function () {
        return my.validateSelected(PROVINCIA) && my.validateSelected(CENTRO);
    };

    my.setData = function (name, value) {
        if ($scope.data && $scope.data[name]) {
            $scope.data[name] = value;
        }
    };

    my.getData = function (name) {
        return $scope.data && $scope.data[name] || '';
    };

    my.clearData = function (name) {
        my.setData(name, '');
    };

    $scope.selectCentro = function () {
        $scope.searchEnabled = true;
    };

    $scope.searchProvincia = function () {
        $scope.centroEnabled = false;
        var value = my.getData(PROVINCIA);
        if (value.length < 1) {
            $scope.provinciaMatches = [];
            $scope.searchProvinciaEnabled = false;
            $scope.searchEnabled = false;
            my.clearData(CENTRO);
            return;
        }

        $scope.searchProvinciaEnabled = true;

        var regex = new RegExp('^' + value);
        var matches = _.filter($scope.provincias, function (n) {
            return regex.test(n);
        });

        $scope.provinciaMatches = matches;
    };

    $scope.selectProvincia = function (provincia) {
        if (! provincia) {
            provincia = ($scope.provinciaMatches.length === 1)  ? $scope.provinciaMatches[0] : false;
            if (! provincia) return;
        }
        my.setData(PROVINCIA, provincia);
        $scope.provinciaMatches = [];
        $scope.searchProvinciaEnabled = false;
        $scope.centroEnabled = true;
        $scope.searchCentros();
    };

    $scope.searchCentros = function () {
        $scope.centroEnabled = my.validateSelected('provincia');
        if ($scope.centroEnabled) {
            var match = _.filter(cache.centros, function (n) {
                return n.provincia === $scope.data.provincia;
            });
            $scope.centros = [];
            if (match.length) {
                $scope.centros = _.uniq(_.pluck(match, 'nombre')).sort();
                $scope.data.centro_examen = '';
                if ($scope.centros.length === 1) {
                    $scope.data.centro_examen = $scope.centros[0];
                }
            }
        }
    };

    $scope.submitSearch = function () {
        if (my.validateAllSelected()) {
            $location.path('/rankings').search($scope.data);
        }
    };

    var fetchAllPromise = CentrosSvc.fetchAll();
    fetchAllPromise.then(function (results) {
        cache.centros = myUtils.promiseDataField(results, 'centros');
        $scope.provincias = _.uniq(_.pluck(cache.centros, PROVINCIA)).sort();
    });

}]).directive('searchPage', ["$timeout", function ($timeout) {
    var resetSelected = function ($elems) {
        $elems.removeClass('selected');
    };

    var selectOne = function ($elem) {
        $elem.addClass('selected');
    };

    return {
        link    : function ($scope, $elem, $attrs) {
            var selectors = {
                'searchProvincia' : '#search-provincia'
            };
            $timeout(function () {
                var selector = '#results-centros li';
                $elem.on('click', selector, function () {
                    $scope.data['centro_examen'] = $(this).text();
                    resetSelected($(selector));
                    selectOne($(this));
                });

                $scope.$watch('centroEnabled', function (newValue, oldValue) {
                    if (newValue) {
                        $(selectors.searchProvincia).blur();
                    }
                });
            })
        }
    };
}]);

angular.module('drivingApp').directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

angular.module('drivingApp').filter('capitalize', function () {
    return function (input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});

angular.module('drivingApp').filter('friendlyCase', ["capitalizeFilter", function (capitalizeFilter) {
    return function (input) {
        input = input.replace('_', ' ');
        return capitalizeFilter(input);
    }
}]);

angular.module('drivingApp').factory('myUtils', function () {

    var api = {};
    api.successMessage = function (message) {
        console.log('"' + message + '" => success :)');
    };

    api.errorMessage = function (message) {
        throw new Error('Wooops: There was an error with the "' + message + '" :(');
    };

    api.promiseDataField = function (response, field, fallback) {
        return api.valueOrFallback(response && response.data && response.data[field], fallback);
    };

    api.valueOrFallback = function (value, fallback) {
        return typeof value === 'undefined' ? fallback : value;
    };

    api.params = function (data) {
        return Object.keys(data).map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
        }).join('&');
    };

    return api;

});

angular.module('drivingApp').service('AptosSrv', ["$http", "$routeParams", "_", function ($http, $routeParams, _) {
    this.fetch = function () {
        return $http.get('/api/search.all', {
            params: $routeParams
        });
    };

    this.fetchSum = function () {
        return $http.get('/api/search.rankings', {
            params: $routeParams
        });
    };

    this.fetchRankings = function (opts) {
        // inclusion criteria
        return $http.get('/api/search.rankings', {
            params: _.extend($routeParams, opts)
        });
    };
}]);




angular.module('drivingApp').service('CentrosSvc', ["$http", "$routeParams", function ($http, $routeParams) {
    var api = {};

    api.fetch = function () {
        return $http.get('/api/centros.search', {
            params: $routeParams
        });
    };

    api.fetchAll = function () {
        return $http.get('/api/centros.all');
    };

    return api;
}]);




angular.module('drivingApp').service('EscuelasSrv', ["$http", "$routeParams", function ($http, $routeParams) {
    this.fetch = function () {
        return $http.get('/api/escuelas.list', {
            params: $routeParams
        });
    };

}]);



//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZHVsZS5qcyIsInJvdXRlcy5qcyIsImNvbnRyb2xsZXJzL2FwdG9zLXN1bS5jdHJsLmpzIiwiY29udHJvbGxlcnMvYXB0b3MuY3RybC5qcyIsImNvbnRyb2xsZXJzL2NlbnRyb3MuY3RybC5qcyIsImNvbnRyb2xsZXJzL2VzY3VlbGFzLmN0cmwuanMiLCJjb250cm9sbGVycy9yYW5raW5ncy5jdHJsLmpzIiwiY29udHJvbGxlcnMvc2VhcmNoLmN0cmwuanMiLCJkaXJlY3RpdmVzL25nRW50ZXIuanMiLCJmaWx0ZXJzL2NhcGl0YWxpemUuanMiLCJmaWx0ZXJzL2ZyaWVuZGx5Q2FzZS5qcyIsInByb3ZpZGVycy91dGlscy5qcyIsInNlcnZpY2VzL2FwdG9zLnN2Yy5qcyIsInNlcnZpY2VzL2NlbnRyb3Muc3ZjLmpzIiwic2VydmljZXMvZXNjdWVsYXMuc3ZjLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0EsUUFBQSxPQUFBLGNBQUE7SUFDQTs7S0FFQSxTQUFBLEtBQUEsT0FBQTtLQUNBLCtCQUFBLFVBQUEsWUFBQSxVQUFBO1FBQ0EsV0FBQSxJQUFBLE9BQUE7UUFDQSxXQUFBLElBQUEsc0JBQUE7Ozs7Ozs7O0FDUEEsUUFBQSxPQUFBO0tBQ0EsK0NBQUEsVUFBQSxnQkFBQSxtQkFBQTtRQUNBO2FBQ0EsS0FBQSxLQUFBO2dCQUNBLFlBQUE7Z0JBQ0EsYUFBQTs7YUFFQSxLQUFBLFdBQUE7Z0JBQ0EsWUFBQTtnQkFDQSxhQUFBOzthQUVBLEtBQUEsZUFBQTtnQkFDQSxZQUFBO2dCQUNBLGFBQUE7O2FBRUEsS0FBQSxvQkFBQTtnQkFDQSxhQUFBO2dCQUNBLGFBQUE7O2FBRUEsS0FBQSxtQkFBQTtnQkFDQSxhQUFBO2dCQUNBLGFBQUE7O2FBRUEsS0FBQSxzQkFBQTtnQkFDQSxhQUFBO2dCQUNBLGFBQUE7O2FBRUEsS0FBQSxhQUFBO2dCQUNBLGFBQUE7Z0JBQ0EsYUFBQTs7YUFFQSxLQUFBLGFBQUE7Z0JBQ0EsYUFBQTtnQkFDQSxhQUFBOzs7UUFHQSxrQkFBQSxVQUFBO1lBQ0EsU0FBQTtZQUNBLGFBQUE7Ozs7O0FDckNBLFFBQUEsT0FBQSxjQUFBLFdBQUEsdURBQUEsVUFBQSxRQUFBLFNBQUEsVUFBQSxHQUFBO0lBQ0EsU0FBQSxXQUFBLEtBQUEsVUFBQSxPQUFBO1FBQ0EsSUFBQSxXQUFBLFFBQUEsaUJBQUEsT0FBQTtRQUNBLElBQUEsUUFBQTtRQUNBLElBQUEsZUFBQSxFQUFBLElBQUEsVUFBQSxVQUFBLEdBQUE7WUFDQSxFQUFBLE9BQUE7WUFDQSxFQUFBLE9BQUEsSUFBQSxLQUFBLEVBQUEsTUFBQSxFQUFBLEtBQUE7WUFDQTtZQUNBLE9BQUE7O1FBRUEsT0FBQSxRQUFBOzs7OztBQ1ZBLFFBQUEsT0FBQSxjQUFBLFdBQUEsb0RBQUEsVUFBQSxRQUFBLFNBQUEsVUFBQSxHQUFBO0lBQ0EsU0FBQSxRQUFBLEtBQUEsVUFBQSxPQUFBO1FBQ0EsSUFBQSxXQUFBLFFBQUEsaUJBQUEsT0FBQTtRQUNBLElBQUEsZUFBQSxFQUFBLElBQUEsVUFBQSxVQUFBLEdBQUE7WUFDQSxFQUFBLE9BQUEsSUFBQSxLQUFBLEVBQUEsTUFBQSxFQUFBLEtBQUE7WUFDQSxPQUFBOztRQUVBLE9BQUEsUUFBQTs7Ozs7QUNQQSxRQUFBLE9BQUEsY0FBQSxXQUFBLHdEQUFBLFVBQUEsUUFBQSxTQUFBLFlBQUEsR0FBQTtJQUNBLFdBQUEsV0FBQSxLQUFBLFVBQUEsU0FBQTtRQUNBLElBQUEsVUFBQSxRQUFBLGlCQUFBLFNBQUE7UUFDQSxPQUFBLFVBQUE7UUFDQSxPQUFBLGFBQUEsRUFBQSxPQUFBLEVBQUEsTUFBQSxTQUFBLGFBQUE7Ozs7O0FDSkEsUUFBQSxPQUFBLGNBQUEsV0FBQSxxREFBQSxVQUFBLFFBQUEsU0FBQSxhQUFBO0lBQ0EsWUFBQSxRQUFBLEtBQUEsVUFBQSxTQUFBO1FBQ0EsT0FBQSxXQUFBLFFBQUEsaUJBQUEsU0FBQTtRQUNBLE9BQUEsUUFBQSxRQUFBLGlCQUFBLFNBQUE7Ozs7QUNKQSxRQUFBLE9BQUEsY0FBQSxXQUFBLGlGQUFBLFVBQUEsUUFBQSxTQUFBLFVBQUEsR0FBQSxjQUFBLFFBQUE7SUFDQSxJQUFBLE9BQUE7UUFDQSxnQkFBQTtRQUNBLGdCQUFBO1FBQ0EsZ0JBQUE7UUFDQSxnQkFBQTtRQUNBLGdCQUFBO1FBQ0EsZ0JBQUE7OztJQUdBLElBQUEsa0JBQUE7UUFDQSxPQUFBO1FBQ0EsT0FBQTtRQUNBLFdBQUE7UUFDQSxlQUFBO1FBQ0EsT0FBQTtRQUNBLE1BQUE7UUFDQSxXQUFBO1FBQ0EsYUFBQTtRQUNBLGdCQUFBOzs7SUFHQSxTQUFBLGdCQUFBLE9BQUE7UUFDQSxJQUFBLFdBQUEsUUFBQSxpQkFBQSxPQUFBO1FBQ0EsSUFBQSxRQUFBO1FBQ0EsSUFBQSxlQUFBLEVBQUEsSUFBQSxVQUFBLFVBQUEsR0FBQTtZQUNBLEVBQUEsT0FBQTtZQUNBLEVBQUEsT0FBQSxJQUFBLEtBQUEsRUFBQSxNQUFBLEVBQUEsS0FBQTtZQUNBO1lBQ0EsT0FBQTs7UUFFQSxPQUFBLFFBQUE7UUFDQSxPQUFBLFNBQUEsUUFBQSxpQkFBQSxPQUFBOztRQUVBLElBQUEsbUJBQUE7UUFDQSxFQUFBLFFBQUEsT0FBQSxRQUFBLFVBQUEsR0FBQSxLQUFBO1lBQ0EsaUJBQUEsZ0JBQUEsUUFBQSxPQUFBOztRQUVBLE9BQUEsYUFBQTs7O0lBR0EsT0FBQSxxQkFBQSxZQUFBO1FBQ0EsT0FBQSxtQkFBQSxDQUFBLE9BQUEsb0JBQUEsUUFBQTs7O0lBR0EsSUFBQSxPQUFBLEVBQUEsT0FBQSxJQUFBLGNBQUE7SUFDQSxTQUFBLGNBQUEsTUFBQSxLQUFBO0lBQ0EsT0FBQSxPQUFBOztJQUVBLE9BQUEsY0FBQSxZQUFBO1FBQ0EsT0FBQTs7O0lBR0EsT0FBQSxlQUFBLFlBQUE7UUFDQSxTQUFBLGNBQUEsT0FBQSxNQUFBLEtBQUE7UUFDQSxPQUFBOzs7O0FDdkRBLFFBQUEsT0FBQSxjQUFBLFdBQUEsb0VBQUEsVUFBQSxRQUFBLFdBQUEsU0FBQSxZQUFBLEdBQUE7O0lBRUEsTUFBQSxZQUFBO0lBQ0EsTUFBQSxTQUFBOztJQUVBLElBQUEsS0FBQTtRQUNBLFFBQUE7O0lBRUEsR0FBQSxtQkFBQSxVQUFBLE1BQUE7UUFDQSxPQUFBLENBQUEsRUFBQSxPQUFBLFFBQUEsT0FBQSxLQUFBOzs7SUFHQSxHQUFBLHNCQUFBLFlBQUE7UUFDQSxPQUFBLEdBQUEsaUJBQUEsY0FBQSxHQUFBLGlCQUFBOzs7SUFHQSxHQUFBLFVBQUEsVUFBQSxNQUFBLE9BQUE7UUFDQSxJQUFBLE9BQUEsUUFBQSxPQUFBLEtBQUEsT0FBQTtZQUNBLE9BQUEsS0FBQSxRQUFBOzs7O0lBSUEsR0FBQSxVQUFBLFVBQUEsTUFBQTtRQUNBLE9BQUEsT0FBQSxRQUFBLE9BQUEsS0FBQSxTQUFBOzs7SUFHQSxHQUFBLFlBQUEsVUFBQSxNQUFBO1FBQ0EsR0FBQSxRQUFBLE1BQUE7OztJQUdBLE9BQUEsZUFBQSxZQUFBO1FBQ0EsT0FBQSxnQkFBQTs7O0lBR0EsT0FBQSxrQkFBQSxZQUFBO1FBQ0EsT0FBQSxnQkFBQTtRQUNBLElBQUEsUUFBQSxHQUFBLFFBQUE7UUFDQSxJQUFBLE1BQUEsU0FBQSxHQUFBO1lBQ0EsT0FBQSxtQkFBQTtZQUNBLE9BQUEseUJBQUE7WUFDQSxPQUFBLGdCQUFBO1lBQ0EsR0FBQSxVQUFBO1lBQ0E7OztRQUdBLE9BQUEseUJBQUE7O1FBRUEsSUFBQSxRQUFBLElBQUEsT0FBQSxNQUFBO1FBQ0EsSUFBQSxVQUFBLEVBQUEsT0FBQSxPQUFBLFlBQUEsVUFBQSxHQUFBO1lBQ0EsT0FBQSxNQUFBLEtBQUE7OztRQUdBLE9BQUEsbUJBQUE7OztJQUdBLE9BQUEsa0JBQUEsVUFBQSxXQUFBO1FBQ0EsSUFBQSxFQUFBLFdBQUE7WUFDQSxZQUFBLENBQUEsT0FBQSxpQkFBQSxXQUFBLE1BQUEsT0FBQSxpQkFBQSxLQUFBO1lBQ0EsSUFBQSxFQUFBLFdBQUE7O1FBRUEsR0FBQSxRQUFBLFdBQUE7UUFDQSxPQUFBLG1CQUFBO1FBQ0EsT0FBQSx5QkFBQTtRQUNBLE9BQUEsZ0JBQUE7UUFDQSxPQUFBOzs7SUFHQSxPQUFBLGdCQUFBLFlBQUE7UUFDQSxPQUFBLGdCQUFBLEdBQUEsaUJBQUE7UUFDQSxJQUFBLE9BQUEsZUFBQTtZQUNBLElBQUEsUUFBQSxFQUFBLE9BQUEsTUFBQSxTQUFBLFVBQUEsR0FBQTtnQkFDQSxPQUFBLEVBQUEsY0FBQSxPQUFBLEtBQUE7O1lBRUEsT0FBQSxVQUFBO1lBQ0EsSUFBQSxNQUFBLFFBQUE7Z0JBQ0EsT0FBQSxVQUFBLEVBQUEsS0FBQSxFQUFBLE1BQUEsT0FBQSxXQUFBO2dCQUNBLE9BQUEsS0FBQSxnQkFBQTtnQkFDQSxJQUFBLE9BQUEsUUFBQSxXQUFBLEdBQUE7b0JBQ0EsT0FBQSxLQUFBLGdCQUFBLE9BQUEsUUFBQTs7Ozs7O0lBTUEsT0FBQSxlQUFBLFlBQUE7UUFDQSxJQUFBLEdBQUEsdUJBQUE7WUFDQSxVQUFBLEtBQUEsYUFBQSxPQUFBLE9BQUE7Ozs7SUFJQSxJQUFBLGtCQUFBLFdBQUE7SUFDQSxnQkFBQSxLQUFBLFVBQUEsU0FBQTtRQUNBLE1BQUEsVUFBQSxRQUFBLGlCQUFBLFNBQUE7UUFDQSxPQUFBLGFBQUEsRUFBQSxLQUFBLEVBQUEsTUFBQSxNQUFBLFNBQUEsWUFBQTs7O0lBR0EsVUFBQSwyQkFBQSxVQUFBLFVBQUE7SUFDQSxJQUFBLGdCQUFBLFVBQUEsUUFBQTtRQUNBLE9BQUEsWUFBQTs7O0lBR0EsSUFBQSxZQUFBLFVBQUEsT0FBQTtRQUNBLE1BQUEsU0FBQTs7O0lBR0EsT0FBQTtRQUNBLFVBQUEsVUFBQSxRQUFBLE9BQUEsUUFBQTtZQUNBLElBQUEsWUFBQTtnQkFDQSxvQkFBQTs7WUFFQSxTQUFBLFlBQUE7Z0JBQ0EsSUFBQSxXQUFBO2dCQUNBLE1BQUEsR0FBQSxTQUFBLFVBQUEsWUFBQTtvQkFDQSxPQUFBLEtBQUEsbUJBQUEsRUFBQSxNQUFBO29CQUNBLGNBQUEsRUFBQTtvQkFDQSxVQUFBLEVBQUE7OztnQkFHQSxPQUFBLE9BQUEsaUJBQUEsVUFBQSxVQUFBLFVBQUE7b0JBQ0EsSUFBQSxVQUFBO3dCQUNBLEVBQUEsVUFBQSxpQkFBQTs7Ozs7Ozs7QUN4SEEsUUFBQSxPQUFBLGNBQUEsVUFBQSxXQUFBLFlBQUE7SUFDQSxPQUFBLFVBQUEsT0FBQSxTQUFBLE9BQUE7UUFDQSxRQUFBLEtBQUEsb0JBQUEsVUFBQSxPQUFBO1lBQ0EsSUFBQSxNQUFBLFVBQUEsSUFBQTtnQkFDQSxNQUFBLE9BQUEsWUFBQTtvQkFDQSxNQUFBLE1BQUEsTUFBQTs7O2dCQUdBLE1BQUE7Ozs7OztBQ1JBLFFBQUEsT0FBQSxjQUFBLE9BQUEsY0FBQSxZQUFBO0lBQ0EsT0FBQSxVQUFBLE9BQUEsS0FBQTtRQUNBLE9BQUEsQ0FBQSxDQUFBLENBQUEsU0FBQSxNQUFBLFFBQUEsdUJBQUEsVUFBQSxLQUFBO1lBQ0EsT0FBQSxJQUFBLE9BQUEsR0FBQSxnQkFBQSxJQUFBLE9BQUEsR0FBQTthQUNBOzs7O0FDSkEsUUFBQSxPQUFBLGNBQUEsT0FBQSxxQ0FBQSxVQUFBLGtCQUFBO0lBQ0EsT0FBQSxVQUFBLE9BQUE7UUFDQSxRQUFBLE1BQUEsUUFBQSxLQUFBO1FBQ0EsT0FBQSxpQkFBQTs7OztBQ0hBLFFBQUEsT0FBQSxjQUFBLFFBQUEsV0FBQSxZQUFBOztJQUVBLElBQUEsTUFBQTtJQUNBLElBQUEsaUJBQUEsVUFBQSxTQUFBO1FBQ0EsUUFBQSxJQUFBLE1BQUEsVUFBQTs7O0lBR0EsSUFBQSxlQUFBLFVBQUEsU0FBQTtRQUNBLE1BQUEsSUFBQSxNQUFBLDBDQUFBLFVBQUE7OztJQUdBLElBQUEsbUJBQUEsVUFBQSxVQUFBLE9BQUEsVUFBQTtRQUNBLE9BQUEsSUFBQSxnQkFBQSxZQUFBLFNBQUEsUUFBQSxTQUFBLEtBQUEsUUFBQTs7O0lBR0EsSUFBQSxrQkFBQSxVQUFBLE9BQUEsVUFBQTtRQUNBLE9BQUEsT0FBQSxVQUFBLGNBQUEsV0FBQTs7O0lBR0EsSUFBQSxTQUFBLFVBQUEsTUFBQTtRQUNBLE9BQUEsT0FBQSxLQUFBLE1BQUEsSUFBQSxVQUFBLEdBQUE7WUFDQSxPQUFBLG1CQUFBLEtBQUEsTUFBQSxtQkFBQSxLQUFBO1dBQ0EsS0FBQTs7O0lBR0EsT0FBQTs7OztBQ3pCQSxRQUFBLE9BQUEsY0FBQSxRQUFBLDJDQUFBLFVBQUEsT0FBQSxjQUFBLEdBQUE7SUFDQSxLQUFBLFFBQUEsWUFBQTtRQUNBLE9BQUEsTUFBQSxJQUFBLG1CQUFBO1lBQ0EsUUFBQTs7OztJQUlBLEtBQUEsV0FBQSxZQUFBO1FBQ0EsT0FBQSxNQUFBLElBQUEsd0JBQUE7WUFDQSxRQUFBOzs7O0lBSUEsS0FBQSxnQkFBQSxVQUFBLE1BQUE7O1FBRUEsT0FBQSxNQUFBLElBQUEsd0JBQUE7WUFDQSxRQUFBLEVBQUEsT0FBQSxjQUFBOzs7Ozs7OztBQ2ZBLFFBQUEsT0FBQSxjQUFBLFFBQUEsd0NBQUEsVUFBQSxPQUFBLGNBQUE7SUFDQSxJQUFBLE1BQUE7O0lBRUEsSUFBQSxRQUFBLFlBQUE7UUFDQSxPQUFBLE1BQUEsSUFBQSx1QkFBQTtZQUNBLFFBQUE7Ozs7SUFJQSxJQUFBLFdBQUEsWUFBQTtRQUNBLE9BQUEsTUFBQSxJQUFBOzs7SUFHQSxPQUFBOzs7Ozs7QUNiQSxRQUFBLE9BQUEsY0FBQSxRQUFBLHlDQUFBLFVBQUEsT0FBQSxjQUFBO0lBQ0EsS0FBQSxRQUFBLFlBQUE7UUFDQSxPQUFBLE1BQUEsSUFBQSxzQkFBQTtZQUNBLFFBQUE7Ozs7Ozs7QUFPQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbmFuZ3VsYXIubW9kdWxlKCdkcml2aW5nQXBwJywgW1xuICAgICduZ1JvdXRlJ1xuXSlcbiAgICAuY29uc3RhbnQoJ18nLCB3aW5kb3cuXylcbiAgICAucnVuKGZ1bmN0aW9uICgkcm9vdFNjb3BlLCAkdGltZW91dCkge1xuICAgICAgICAkcm9vdFNjb3BlLl8gPSB3aW5kb3cuXztcbiAgICAgICAgJHJvb3RTY29wZS4kb24oJyR2aWV3Q29udGVudExvYWRlZCcsICgpPT4ge1xuICAgICAgICAgICAgJHRpbWVvdXQoKCk9PiB7XG4gICAgICAgICAgICAgICAgY29tcG9uZW50SGFuZGxlci51cGdyYWRlQWxsUmVnaXN0ZXJlZCgpO1xuICAgICAgICB9KTtcbiAgICB9KVxuXG4gICAgfSk7XG4iLCJhbmd1bGFyLm1vZHVsZSgnZHJpdmluZ0FwcCcpXG4gICAgLmNvbmZpZyhmdW5jdGlvbiAoJHJvdXRlUHJvdmlkZXIsICRsb2NhdGlvblByb3ZpZGVyKSB7XG4gICAgICAgICRyb3V0ZVByb3ZpZGVyXG4gICAgICAgICAgICAud2hlbignLycsIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiAnU2VhcmNoQ3RybCcsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdzZWFyY2guaHRtbCdcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbignL3NlYXJjaCcsIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiAnU2VhcmNoQ3RybCcsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdzZWFyY2guaHRtbCdcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbignL3NlYXJjaC5hbGwnLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogJ0FwdG9zQ3RybCcsXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdzZWFyY2gtYWxsLmh0bWwnXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLndoZW4oJy9zZWFyY2gucmFua2luZ3MnLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlciA6ICdBcHRvc1N1bUN0cmwnLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnYXB0b3Mtc3VtLmh0bWwnXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLndoZW4oJy9zZWFyY2guY2VudHJvcycsIHtcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyIDogJ0NlbnRyb3NDdHJsJyxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ2NlbnRyb3MuaHRtbCdcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbignL3NlYXJjaC5wcm92aW5jaWFzJywge1xuICAgICAgICAgICAgICAgIGNvbnRyb2xsZXIgOiAnQ2VudHJvc0N0cmwnLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncHJvdmluY2lhcy5odG1sJ1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC53aGVuKCcvZXNjdWVsYXMnLCB7XG4gICAgICAgICAgICAgICAgY29udHJvbGxlciA6ICdFc2N1ZWxhc0N0cmwnLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnZXNjdWVsYXMuaHRtbCdcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAud2hlbignL3JhbmtpbmdzJywge1xuICAgICAgICAgICAgICAgIGNvbnRyb2xsZXIgOiAnUmFua2luZ3NDdHJsJyxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3JhbmtpbmdzLmh0bWwnXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAkbG9jYXRpb25Qcm92aWRlci5odG1sNU1vZGUoe1xuICAgICAgICAgICAgZW5hYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgIHJlcXVpcmVCYXNlOiBmYWxzZVxuICAgICAgICB9KTtcbiAgICB9KTtcbiIsIlxuYW5ndWxhci5tb2R1bGUoJ2RyaXZpbmdBcHAnKS5jb250cm9sbGVyKCdBcHRvc1N1bUN0cmwnLCBmdW5jdGlvbiAoJHNjb3BlLCBteVV0aWxzLCBBcHRvc1NydiwgXykge1xuICAgIEFwdG9zU3J2LmZldGNoU3VtKCkudGhlbihmdW5jdGlvbiAocG9zdHMpIHtcbiAgICAgICAgdmFyIHJhd1Bvc3RzID0gbXlVdGlscy5wcm9taXNlRGF0YUZpZWxkKHBvc3RzLCAnYXB0b3MnKTtcbiAgICAgICAgdmFyIGNvdW50ID0gMTtcbiAgICAgICAgdmFyIHByZXBwZWRQb3N0cyA9IF8ubWFwKHJhd1Bvc3RzLCBmdW5jdGlvbiAobikge1xuICAgICAgICAgICAgbi5yYW5rID0gY291bnQ7XG4gICAgICAgICAgICBuLmRhdGUgPSBuZXcgRGF0ZShuLmFueW8sIG4ubWVzLCAxKTtcbiAgICAgICAgICAgIGNvdW50Kys7XG4gICAgICAgICAgICByZXR1cm4gbjtcbiAgICAgICAgfSk7XG4gICAgICAgICRzY29wZS5hcHRvcyA9IHByZXBwZWRQb3N0cztcbiAgICB9KTtcbn0pO1xuIiwiXG5hbmd1bGFyLm1vZHVsZSgnZHJpdmluZ0FwcCcpLmNvbnRyb2xsZXIoJ0FwdG9zQ3RybCcsIGZ1bmN0aW9uICgkc2NvcGUsIG15VXRpbHMsIEFwdG9zU3J2LCBfKSB7XG4gICAgQXB0b3NTcnYuZmV0Y2goKS50aGVuKGZ1bmN0aW9uIChwb3N0cykge1xuICAgICAgICB2YXIgcmF3UG9zdHMgPSBteVV0aWxzLnByb21pc2VEYXRhRmllbGQocG9zdHMsICdhcHRvcycpO1xuICAgICAgICB2YXIgcHJlcHBlZFBvc3RzID0gXy5tYXAocmF3UG9zdHMsIGZ1bmN0aW9uIChuKSB7XG4gICAgICAgICAgICBuLmRhdGUgPSBuZXcgRGF0ZShuLmFueW8sIG4ubWVzLCAxKTtcbiAgICAgICAgICAgIHJldHVybiBuO1xuICAgICAgICB9KTtcbiAgICAgICAgJHNjb3BlLmFwdG9zID0gcHJlcHBlZFBvc3RzO1xuICAgIH0pO1xufSk7XG4iLCJcbmFuZ3VsYXIubW9kdWxlKCdkcml2aW5nQXBwJykuY29udHJvbGxlcignQ2VudHJvc0N0cmwnLCBmdW5jdGlvbiAoJHNjb3BlLCBteVV0aWxzLCBDZW50cm9zU3ZjLCBfKSB7XG4gICAgQ2VudHJvc1N2Yy5mZXRjaEFsbCgpLnRoZW4oZnVuY3Rpb24gKHJlc3VsdHMpIHtcbiAgICAgICAgdmFyIGNlbnRyb3MgPSBteVV0aWxzLnByb21pc2VEYXRhRmllbGQocmVzdWx0cywgJ2NlbnRyb3MnKTtcbiAgICAgICAgJHNjb3BlLmNlbnRyb3MgPSBjZW50cm9zO1xuICAgICAgICAkc2NvcGUucHJvdmluY2lhcyA9IF8udW5pcXVlKF8ucGx1Y2soY2VudHJvcywgJ3Byb3ZpbmNpYScpLnNvcnQoKSk7XG4gICAgfSk7XG59KTtcbiIsIlxuYW5ndWxhci5tb2R1bGUoJ2RyaXZpbmdBcHAnKS5jb250cm9sbGVyKCdFc2N1ZWxhc0N0cmwnLCBmdW5jdGlvbiAoJHNjb3BlLCBteVV0aWxzLCBFc2N1ZWxhc1Nydikge1xuICAgIEVzY3VlbGFzU3J2LmZldGNoKCkudGhlbihmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgICAkc2NvcGUuZXNjdWVsYXMgPSBteVV0aWxzLnByb21pc2VEYXRhRmllbGQocmVzdWx0cywgJ2VzY3VlbGFzJyk7XG4gICAgICAgICRzY29wZS5jb3VudCA9IG15VXRpbHMucHJvbWlzZURhdGFGaWVsZChyZXN1bHRzLCAnY291bnQnKTtcbiAgICB9KTtcbn0pO1xuIiwiYW5ndWxhci5tb2R1bGUoJ2RyaXZpbmdBcHAnKS5jb250cm9sbGVyKCdSYW5raW5nc0N0cmwnLCBmdW5jdGlvbiAoJHNjb3BlLCBteVV0aWxzLCBBcHRvc1NydiwgXywgJHJvdXRlUGFyYW1zLCAkcm91dGUpIHtcbiAgICB2YXIgb3B0cyA9IHtcbiAgICAgICAgbWluX2FwdG9zICAgICA6IDIwLCAvLyBtaW5pbXVtIG51bWJlciBvZiBhcHBsaWNhdGlvbnNcbiAgICAgICAgbGltaXQgICAgICAgICA6IDEwMCxcbiAgICAgICAgbWVzX2IgICAgICAgICA6IDcsXG4gICAgICAgIGFueW8gICAgICAgICAgOiAyMDE1LFxuICAgICAgICB0aXBvX2V4YW1lbiAgIDogJ0NvbmR1Y2Npw7NuIFkgQ2lyY3VsYWNpw7NuJyxcbiAgICAgICAgbm9tYnJlX3Blcm1pc286ICdiJyAvLyBtYXggbnVtYmVyIG9mIHJlc3VsdHNcbiAgICB9O1xuXG4gICAgdmFyIHRyYW5zbGF0ZVBhcmFtcyA9IHtcbiAgICAgICAgbWVzX2E6ICdEZSBNZXMnLFxuICAgICAgICBtZXNfYjogJ0EgTWVzJyxcbiAgICAgICAgbWluX2FwdG9zOiAnTWluaW1vIEFwdG9zJyxcbiAgICAgICAgY2VudHJvX2V4YW1lbjogJ0NlbnRybyBkZSBFeGFtZW4nLFxuICAgICAgICBsaW1pdDogJ0xpbWl0ZSBkZSBSZXN1bHRhZG9zJyxcbiAgICAgICAgYW55bzogJ0HDsW8nLFxuICAgICAgICBwcm92aW5jaWE6ICdQcm92aW5jaWEnLFxuICAgICAgICB0aXBvX2V4YW1lbjogJ1RpcG8gRXhhbWVuJyxcbiAgICAgICAgbm9tYnJlX3Blcm1pc286ICdOb21icmUgUGVybWlzbydcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gcmFua2luZ3NTdWNjZXNzKHBvc3RzKSB7XG4gICAgICAgIHZhciByYXdQb3N0cyA9IG15VXRpbHMucHJvbWlzZURhdGFGaWVsZChwb3N0cywgJ2FwdG9zJyk7XG4gICAgICAgIHZhciBjb3VudCA9IDE7XG4gICAgICAgIHZhciBwcmVwcGVkUG9zdHMgPSBfLm1hcChyYXdQb3N0cywgZnVuY3Rpb24gKG4pIHtcbiAgICAgICAgICAgIG4ucmFuayA9IGNvdW50O1xuICAgICAgICAgICAgbi5kYXRlID0gbmV3IERhdGUobi5hbnlvLCBuLm1lcywgMSk7XG4gICAgICAgICAgICBjb3VudCsrO1xuICAgICAgICAgICAgcmV0dXJuIG47XG4gICAgICAgIH0pO1xuICAgICAgICAkc2NvcGUuYXB0b3MgPSBwcmVwcGVkUG9zdHM7XG4gICAgICAgICRzY29wZS5wYXJhbXMgPSBteVV0aWxzLnByb21pc2VEYXRhRmllbGQocG9zdHMsICdwYXJhbXMnKTtcblxuICAgICAgICB2YXIgdHJhbnNsYXRlZFBhcmFtcyA9IHt9O1xuICAgICAgICBfLmZvckVhY2goJHNjb3BlLnBhcmFtcywgZnVuY3Rpb24gKG4sIGtleSkge1xuICAgICAgICAgICAgdHJhbnNsYXRlZFBhcmFtc1t0cmFuc2xhdGVQYXJhbXNba2V5XSB8fCBrZXldID0gbjtcbiAgICAgICAgfSk7XG4gICAgICAgICRzY29wZS5uaWNlUGFyYW1zID0gdHJhbnNsYXRlZFBhcmFtcztcbiAgICB9XG5cbiAgICAkc2NvcGUudG9nZ2xlRmlsdGVySW5wdXRzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAkc2NvcGUuc2hvd0ZpbHRlcklucHV0cyA9ICgkc2NvcGUuc2hvd0ZpbHRlcklucHV0cykgPyBmYWxzZSA6IHRydWU7XG4gICAgfTtcblxuICAgIHZhciBkYXRhID0gXy5leHRlbmQoe30sICRyb3V0ZVBhcmFtcywgb3B0cyk7XG4gICAgQXB0b3NTcnYuZmV0Y2hSYW5raW5ncyhvcHRzKS50aGVuKHJhbmtpbmdzU3VjY2Vzcyk7XG4gICAgJHNjb3BlLmRhdGEgPSBkYXRhO1xuXG4gICAgJHNjb3BlLnJlbG9hZFJvdXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAkcm91dGUucmVsb2FkKCk7XG4gICAgfTtcblxuICAgICRzY29wZS5zdWJtaXRTZWFyY2ggPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIEFwdG9zU3J2LmZldGNoUmFua2luZ3MoJHNjb3BlLmRhdGEpLnRoZW4ocmFua2luZ3NTdWNjZXNzKTtcbiAgICAgICAgJHNjb3BlLnRvZ2dsZUZpbHRlcklucHV0cygpO1xuICAgIH07XG59KTtcbiIsImFuZ3VsYXIubW9kdWxlKCdkcml2aW5nQXBwJykuY29udHJvbGxlcignU2VhcmNoQ3RybCcsIGZ1bmN0aW9uICgkc2NvcGUsICRsb2NhdGlvbiwgbXlVdGlscywgQ2VudHJvc1N2YywgXykge1xuXG4gICAgY29uc3QgUFJPVklOQ0lBID0gJ3Byb3ZpbmNpYSc7XG4gICAgY29uc3QgQ0VOVFJPID0gJ2NlbnRyb19leGFtZW4nO1xuXG4gICAgdmFyIG15ID0ge30sXG4gICAgICAgIGNhY2hlID0ge307XG5cbiAgICBteS52YWxpZGF0ZVNlbGVjdGVkID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgcmV0dXJuICEhKCRzY29wZS5kYXRhICYmICRzY29wZS5kYXRhW25hbWVdKVxuICAgIH07XG5cbiAgICBteS52YWxpZGF0ZUFsbFNlbGVjdGVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gbXkudmFsaWRhdGVTZWxlY3RlZChQUk9WSU5DSUEpICYmIG15LnZhbGlkYXRlU2VsZWN0ZWQoQ0VOVFJPKTtcbiAgICB9O1xuXG4gICAgbXkuc2V0RGF0YSA9IGZ1bmN0aW9uIChuYW1lLCB2YWx1ZSkge1xuICAgICAgICBpZiAoJHNjb3BlLmRhdGEgJiYgJHNjb3BlLmRhdGFbbmFtZV0pIHtcbiAgICAgICAgICAgICRzY29wZS5kYXRhW25hbWVdID0gdmFsdWU7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgbXkuZ2V0RGF0YSA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgIHJldHVybiAkc2NvcGUuZGF0YSAmJiAkc2NvcGUuZGF0YVtuYW1lXSB8fCAnJztcbiAgICB9O1xuXG4gICAgbXkuY2xlYXJEYXRhID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgbXkuc2V0RGF0YShuYW1lLCAnJyk7XG4gICAgfTtcblxuICAgICRzY29wZS5zZWxlY3RDZW50cm8gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICRzY29wZS5zZWFyY2hFbmFibGVkID0gdHJ1ZTtcbiAgICB9O1xuXG4gICAgJHNjb3BlLnNlYXJjaFByb3ZpbmNpYSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJHNjb3BlLmNlbnRyb0VuYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgdmFyIHZhbHVlID0gbXkuZ2V0RGF0YShQUk9WSU5DSUEpO1xuICAgICAgICBpZiAodmFsdWUubGVuZ3RoIDwgMSkge1xuICAgICAgICAgICAgJHNjb3BlLnByb3ZpbmNpYU1hdGNoZXMgPSBbXTtcbiAgICAgICAgICAgICRzY29wZS5zZWFyY2hQcm92aW5jaWFFbmFibGVkID0gZmFsc2U7XG4gICAgICAgICAgICAkc2NvcGUuc2VhcmNoRW5hYmxlZCA9IGZhbHNlO1xuICAgICAgICAgICAgbXkuY2xlYXJEYXRhKENFTlRSTyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAkc2NvcGUuc2VhcmNoUHJvdmluY2lhRW5hYmxlZCA9IHRydWU7XG5cbiAgICAgICAgdmFyIHJlZ2V4ID0gbmV3IFJlZ0V4cCgnXicgKyB2YWx1ZSk7XG4gICAgICAgIHZhciBtYXRjaGVzID0gXy5maWx0ZXIoJHNjb3BlLnByb3ZpbmNpYXMsIGZ1bmN0aW9uIChuKSB7XG4gICAgICAgICAgICByZXR1cm4gcmVnZXgudGVzdChuKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJHNjb3BlLnByb3ZpbmNpYU1hdGNoZXMgPSBtYXRjaGVzO1xuICAgIH07XG5cbiAgICAkc2NvcGUuc2VsZWN0UHJvdmluY2lhID0gZnVuY3Rpb24gKHByb3ZpbmNpYSkge1xuICAgICAgICBpZiAoISBwcm92aW5jaWEpIHtcbiAgICAgICAgICAgIHByb3ZpbmNpYSA9ICgkc2NvcGUucHJvdmluY2lhTWF0Y2hlcy5sZW5ndGggPT09IDEpICA/ICRzY29wZS5wcm92aW5jaWFNYXRjaGVzWzBdIDogZmFsc2U7XG4gICAgICAgICAgICBpZiAoISBwcm92aW5jaWEpIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBteS5zZXREYXRhKFBST1ZJTkNJQSwgcHJvdmluY2lhKTtcbiAgICAgICAgJHNjb3BlLnByb3ZpbmNpYU1hdGNoZXMgPSBbXTtcbiAgICAgICAgJHNjb3BlLnNlYXJjaFByb3ZpbmNpYUVuYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgJHNjb3BlLmNlbnRyb0VuYWJsZWQgPSB0cnVlO1xuICAgICAgICAkc2NvcGUuc2VhcmNoQ2VudHJvcygpO1xuICAgIH07XG5cbiAgICAkc2NvcGUuc2VhcmNoQ2VudHJvcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJHNjb3BlLmNlbnRyb0VuYWJsZWQgPSBteS52YWxpZGF0ZVNlbGVjdGVkKCdwcm92aW5jaWEnKTtcbiAgICAgICAgaWYgKCRzY29wZS5jZW50cm9FbmFibGVkKSB7XG4gICAgICAgICAgICB2YXIgbWF0Y2ggPSBfLmZpbHRlcihjYWNoZS5jZW50cm9zLCBmdW5jdGlvbiAobikge1xuICAgICAgICAgICAgICAgIHJldHVybiBuLnByb3ZpbmNpYSA9PT0gJHNjb3BlLmRhdGEucHJvdmluY2lhO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkc2NvcGUuY2VudHJvcyA9IFtdO1xuICAgICAgICAgICAgaWYgKG1hdGNoLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5jZW50cm9zID0gXy51bmlxKF8ucGx1Y2sobWF0Y2gsICdub21icmUnKSkuc29ydCgpO1xuICAgICAgICAgICAgICAgICRzY29wZS5kYXRhLmNlbnRyb19leGFtZW4gPSAnJztcbiAgICAgICAgICAgICAgICBpZiAoJHNjb3BlLmNlbnRyb3MubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5kYXRhLmNlbnRyb19leGFtZW4gPSAkc2NvcGUuY2VudHJvc1swXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgJHNjb3BlLnN1Ym1pdFNlYXJjaCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKG15LnZhbGlkYXRlQWxsU2VsZWN0ZWQoKSkge1xuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9yYW5raW5ncycpLnNlYXJjaCgkc2NvcGUuZGF0YSk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIGZldGNoQWxsUHJvbWlzZSA9IENlbnRyb3NTdmMuZmV0Y2hBbGwoKTtcbiAgICBmZXRjaEFsbFByb21pc2UudGhlbihmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgICBjYWNoZS5jZW50cm9zID0gbXlVdGlscy5wcm9taXNlRGF0YUZpZWxkKHJlc3VsdHMsICdjZW50cm9zJyk7XG4gICAgICAgICRzY29wZS5wcm92aW5jaWFzID0gXy51bmlxKF8ucGx1Y2soY2FjaGUuY2VudHJvcywgUFJPVklOQ0lBKSkuc29ydCgpO1xuICAgIH0pO1xuXG59KS5kaXJlY3RpdmUoJ3NlYXJjaFBhZ2UnLCBmdW5jdGlvbiAoJHRpbWVvdXQpIHtcbiAgICB2YXIgcmVzZXRTZWxlY3RlZCA9IGZ1bmN0aW9uICgkZWxlbXMpIHtcbiAgICAgICAgJGVsZW1zLnJlbW92ZUNsYXNzKCdzZWxlY3RlZCcpO1xuICAgIH07XG5cbiAgICB2YXIgc2VsZWN0T25lID0gZnVuY3Rpb24gKCRlbGVtKSB7XG4gICAgICAgICRlbGVtLmFkZENsYXNzKCdzZWxlY3RlZCcpO1xuICAgIH07XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBsaW5rICAgIDogZnVuY3Rpb24gKCRzY29wZSwgJGVsZW0sICRhdHRycykge1xuICAgICAgICAgICAgdmFyIHNlbGVjdG9ycyA9IHtcbiAgICAgICAgICAgICAgICAnc2VhcmNoUHJvdmluY2lhJyA6ICcjc2VhcmNoLXByb3ZpbmNpYSdcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICAkdGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNlbGVjdG9yID0gJyNyZXN1bHRzLWNlbnRyb3MgbGknO1xuICAgICAgICAgICAgICAgICRlbGVtLm9uKCdjbGljaycsIHNlbGVjdG9yLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5kYXRhWydjZW50cm9fZXhhbWVuJ10gPSAkKHRoaXMpLnRleHQoKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzZXRTZWxlY3RlZCgkKHNlbGVjdG9yKSk7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdE9uZSgkKHRoaXMpKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICRzY29wZS4kd2F0Y2goJ2NlbnRyb0VuYWJsZWQnLCBmdW5jdGlvbiAobmV3VmFsdWUsIG9sZFZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChuZXdWYWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJChzZWxlY3RvcnMuc2VhcmNoUHJvdmluY2lhKS5ibHVyKCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICB9O1xufSk7XG4iLCJhbmd1bGFyLm1vZHVsZSgnZHJpdmluZ0FwcCcpLmRpcmVjdGl2ZSgnbmdFbnRlcicsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xuICAgICAgICBlbGVtZW50LmJpbmQoXCJrZXlkb3duIGtleXByZXNzXCIsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgaWYgKGV2ZW50LndoaWNoID09PSAxMykge1xuICAgICAgICAgICAgICAgIHNjb3BlLiRhcHBseShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLiRldmFsKGF0dHJzLm5nRW50ZXIpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfTtcbn0pO1xuIiwiYW5ndWxhci5tb2R1bGUoJ2RyaXZpbmdBcHAnKS5maWx0ZXIoJ2NhcGl0YWxpemUnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChpbnB1dCwgYWxsKSB7XG4gICAgICAgIHJldHVybiAoISFpbnB1dCkgPyBpbnB1dC5yZXBsYWNlKC8oW15cXFdfXStbXlxccy1dKikgKi9nLCBmdW5jdGlvbiAodHh0KSB7XG4gICAgICAgICAgICByZXR1cm4gdHh0LmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgdHh0LnN1YnN0cigxKS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICB9KSA6ICcnO1xuICAgIH1cbn0pO1xuIiwiYW5ndWxhci5tb2R1bGUoJ2RyaXZpbmdBcHAnKS5maWx0ZXIoJ2ZyaWVuZGx5Q2FzZScsIGZ1bmN0aW9uIChjYXBpdGFsaXplRmlsdGVyKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChpbnB1dCkge1xuICAgICAgICBpbnB1dCA9IGlucHV0LnJlcGxhY2UoJ18nLCAnICcpO1xuICAgICAgICByZXR1cm4gY2FwaXRhbGl6ZUZpbHRlcihpbnB1dCk7XG4gICAgfVxufSk7XG4iLCJhbmd1bGFyLm1vZHVsZSgnZHJpdmluZ0FwcCcpLmZhY3RvcnkoJ215VXRpbHMnLCBmdW5jdGlvbiAoKSB7XG5cbiAgICB2YXIgYXBpID0ge307XG4gICAgYXBpLnN1Y2Nlc3NNZXNzYWdlID0gZnVuY3Rpb24gKG1lc3NhZ2UpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1wiJyArIG1lc3NhZ2UgKyAnXCIgPT4gc3VjY2VzcyA6KScpO1xuICAgIH07XG5cbiAgICBhcGkuZXJyb3JNZXNzYWdlID0gZnVuY3Rpb24gKG1lc3NhZ2UpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdXb29vcHM6IFRoZXJlIHdhcyBhbiBlcnJvciB3aXRoIHRoZSBcIicgKyBtZXNzYWdlICsgJ1wiIDooJyk7XG4gICAgfTtcblxuICAgIGFwaS5wcm9taXNlRGF0YUZpZWxkID0gZnVuY3Rpb24gKHJlc3BvbnNlLCBmaWVsZCwgZmFsbGJhY2spIHtcbiAgICAgICAgcmV0dXJuIGFwaS52YWx1ZU9yRmFsbGJhY2socmVzcG9uc2UgJiYgcmVzcG9uc2UuZGF0YSAmJiByZXNwb25zZS5kYXRhW2ZpZWxkXSwgZmFsbGJhY2spO1xuICAgIH07XG5cbiAgICBhcGkudmFsdWVPckZhbGxiYWNrID0gZnVuY3Rpb24gKHZhbHVlLCBmYWxsYmFjaykge1xuICAgICAgICByZXR1cm4gdHlwZW9mIHZhbHVlID09PSAndW5kZWZpbmVkJyA/IGZhbGxiYWNrIDogdmFsdWU7XG4gICAgfTtcblxuICAgIGFwaS5wYXJhbXMgPSBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMoZGF0YSkubWFwKGZ1bmN0aW9uIChrKSB7XG4gICAgICAgICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGspICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KGRhdGFba10pXG4gICAgICAgIH0pLmpvaW4oJyYnKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIGFwaTtcblxufSk7XG4iLCJhbmd1bGFyLm1vZHVsZSgnZHJpdmluZ0FwcCcpLnNlcnZpY2UoJ0FwdG9zU3J2JywgZnVuY3Rpb24gKCRodHRwLCAkcm91dGVQYXJhbXMsIF8pIHtcbiAgICB0aGlzLmZldGNoID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL3NlYXJjaC5hbGwnLCB7XG4gICAgICAgICAgICBwYXJhbXM6ICRyb3V0ZVBhcmFtc1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgdGhpcy5mZXRjaFN1bSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwLmdldCgnL2FwaS9zZWFyY2gucmFua2luZ3MnLCB7XG4gICAgICAgICAgICBwYXJhbXM6ICRyb3V0ZVBhcmFtc1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgdGhpcy5mZXRjaFJhbmtpbmdzID0gZnVuY3Rpb24gKG9wdHMpIHtcbiAgICAgICAgLy8gaW5jbHVzaW9uIGNyaXRlcmlhXG4gICAgICAgIHJldHVybiAkaHR0cC5nZXQoJy9hcGkvc2VhcmNoLnJhbmtpbmdzJywge1xuICAgICAgICAgICAgcGFyYW1zOiBfLmV4dGVuZCgkcm91dGVQYXJhbXMsIG9wdHMpXG4gICAgICAgIH0pO1xuICAgIH07XG59KTtcblxuXG4iLCJcbmFuZ3VsYXIubW9kdWxlKCdkcml2aW5nQXBwJykuc2VydmljZSgnQ2VudHJvc1N2YycsIGZ1bmN0aW9uICgkaHR0cCwgJHJvdXRlUGFyYW1zKSB7XG4gICAgdmFyIGFwaSA9IHt9O1xuXG4gICAgYXBpLmZldGNoID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL2NlbnRyb3Muc2VhcmNoJywge1xuICAgICAgICAgICAgcGFyYW1zOiAkcm91dGVQYXJhbXNcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIGFwaS5mZXRjaEFsbCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuICRodHRwLmdldCgnL2FwaS9jZW50cm9zLmFsbCcpO1xuICAgIH07XG5cbiAgICByZXR1cm4gYXBpO1xufSk7XG5cblxuIiwiXG5hbmd1bGFyLm1vZHVsZSgnZHJpdmluZ0FwcCcpLnNlcnZpY2UoJ0VzY3VlbGFzU3J2JywgZnVuY3Rpb24gKCRodHRwLCAkcm91dGVQYXJhbXMpIHtcbiAgICB0aGlzLmZldGNoID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL2VzY3VlbGFzLmxpc3QnLCB7XG4gICAgICAgICAgICBwYXJhbXM6ICRyb3V0ZVBhcmFtc1xuICAgICAgICB9KTtcbiAgICB9O1xuXG59KTtcblxuXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=