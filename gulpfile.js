var gulp = require('gulp'),
    concat = require('gulp-concat'),
    browserify = require('gulp-browserify'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    nodemon = require('gulp-nodemon'),
    notify = require('gulp-notify'),
    sourcemaps = require('gulp-sourcemaps'),
    fs = require('fs'),
    expect = require('chai').expect,
    _ = require('lodash'),
    config = {
        modulePath: './ng',
        bowerDir  : './bower_components',
        public    : {
            js : 'public/js',
            css: 'public/css'
        }
    };

var db = require('./db');

gulp.task('sass', function () {
    // gulp.src locates the source files for the process.
    // This globbing function tells gulp to use all files
    // ending with .scss or .sass within the scss folder.
    gulp.src([
       'sass/app.scss'
    ])
        .pipe(sass({
            includePaths: [
                config.bowerDir + '/foundation/scss'
            ]
        }))
        // Converts Sass into CSS with Gulp Sass
        // Init sourcemaps
        .pipe(sourcemaps.init())
        .pipe(sass({
            errLogToConsole: true
        }))
        // Writes sourcemaps into the CSS file
        .pipe(sourcemaps.write())
        // Outputs CSS files in the css folder
        .pipe(concat('main.css'))
        .pipe(notify(function (file) {
            return "Gulp build: " + file.relative;
        }))
        .pipe(gulp.dest(config.public.css));
});

gulp.task('js:modules', function () {
    return gulp.src([])
        .pipe(plumber())
        .pipe(browserify())
        .pipe(gulp.dest(config.public.js));
});

gulp.task('css', function () {
    gulp.src([
        'css/*'
    ])
        .pipe(concat('main.css'))
        .pipe(gulp.dest(config.public.css));
});

gulp.task('js-front', function () {
    gulp.src([
        config.bowerDir + '/lodash/lodash.js',
        config.bowerDir + '/angular/angular.js',
        config.bowerDir + '/angular-route/angular-route.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('front.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.public.js));

});

gulp.task('server', function () {
    nodemon({
        script: 'server.js',
        ext   : '.js',
        ignore: ['ng*', 'gulp*', 'assets*']
    });
});

gulp.task('js-assets', function () {
    gulp.src([
        'ng/module.js',
        'ng/**/*.js',
        'js/**/*.js'
    ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(sourcemaps.write())
        .pipe(notify(function (file) {
            return "Gulp build: " + file.relative;
        }))
        .pipe(gulp.dest(config.public.js));
});

gulp.task('build', ['js-front', 'js-assets', 'js:modules', 'sass', 'server']);
gulp.task('watch:js', function () {
    gulp.watch('npm/**/*.js', ['js:modules']);
    gulp.watch('ng/**/*.js', ['js-assets']);
    gulp.watch(config.bowerDir + '/**/*.js', ['js-front']);
});
gulp.task('watch:sass', function () {
    gulp.watch('sass/**/*.{scss,sass}', ['sass'])

});
gulp.task('watch:css', ['css'], function () {
    gulp.watch('css/*', ['css']);
});
gulp.task('watch', ['watch:js', 'watch:sass']);

gulp.task('default', ['build', 'watch']);
