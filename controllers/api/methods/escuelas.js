var Autoescuela = require('../../../models/autoescuela'),
    queryBuild = require('../helpers/query-build'),
    jsonRes = require('../helpers/response').json,
    api = {};

api.search = function (req, res, next) {
    var query = Autoescuela.find();
    var searchRegex = {
        nombre: 'nombre',
        codigo: 'codigo'
    };
    console.log(req.query);
    query = queryBuild.queryString(query, req.query, searchRegex);
    query = queryBuild.queryCount(query, req.query, 100);
    query.sort({nombre: 1});
    query.exec(function (err, results) {
        if (err) {
            return next(err);
        }
        res.json(jsonRes.success({params: req.query, field: 'escuelas', response:results}));
    });
};

module.exports = api;