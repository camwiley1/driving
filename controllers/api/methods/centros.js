var CentoExamen = require('../../../models/centro-examen'),
    queryBuild = require('../helpers/query-build'),
    jsonRes = require('../helpers/response').json,
    resField = 'centros',
    api = {},
    my = {};

api.search = function (req, res, next) {
    var query = CentoExamen.find();
    var searchRegex = {
        nombre: 'nombre',
        provincia: 'provincia'
    };
    query = queryBuild.queryString(query, req.query, searchRegex);
    query = queryBuild.queryCount(query, req.query, 100);
    query.sort({nombre: 1});
    query.exec(function (err, results) {
        if (err) {
            return next(err);
        }
        res.json(jsonRes.success({params: req.query, field: resField, response: results}));
    });
};

api.all = function (req, res, next) {
    var query = CentoExamen.find();
    query.sort({nombre: 1});
    query.exec(function (err, results) {
        if (err) {
            return next(err);
        }
        res.json(jsonRes.success({params: req.query, field: resField, response: results}));
    });
};

module.exports = api;