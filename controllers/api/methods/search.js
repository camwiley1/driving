var Apto = require('../../../models/apto'),
    queryBuild = require('../helpers/query-build'),
    _ = require('lodash'),
    jsonRes = require('../helpers/response').json,
    my = {},
    api;

my.all = function (req, res, next) {
    var searchRegex = {
        desc_provincia    : 'provincia',
        centro_examen     : 'centro_examen',
        codigo_autoescuela: 'codigo_autoescuela',
        nombre_autoescuela: 'nombre_autoescuela',
        codigo_seccion    : 'codigo_seccion',
        tipo_examen       : 'tipo_examen',
        nombre_permiso    : 'nombre_permiso'
    };

    var searchInt = {
        mes              : 'mes',
        anyo             : 'anyo',
        num_aptos        : 'num_aptos',
        num_aptos_2conv  : 'num_aptos_2conv',
        num_aptos_3o4conv: 'num_aptos_3o4conv',
        num_aptos_5_o_mas: 'num_aptos_5_o_mas_conv',
        num_no_aptos     : 'num_no_aptos'
    };

    var query = Apto.find();
    query = queryBuild.queryString(query, req.query, searchRegex);
    query = queryBuild.queryInt(query, req.query, searchInt);
    query = queryBuild.queryCount(query, req.query, 100);

    query.exec(function (error, results) {
        var codigos = _.unique(_.pluck(results, 'codigo_autoescuela')).sort();
        res.json(jsonRes.success({query: req.query, field: 'aptos', response: results}));
    });
};

my.rankings = function (req, res, next) {
    var aggregate = [];
    var match = {};
    match.nombre_autoescuela = {
        $exists: true
    };

    if (req.query && req.query.provincia) {
        match.desc_provincia = req.query.provincia;
    }
    if (req.query && req.query.centro_examen) {
        match.centro_examen = req.query.centro_examen;
    }
    if (req.query && req.query.nombre_permiso) {
        match.nombre_permiso = req.query.nombre_permiso;
    }
    if (req.query && req.query.codigo_autoescuela) {
        match.codigo_autoescuela = req.query.codigo_autoescuela;
    }

    if (req.query && req.query.anyo) {
        match.anyo = parseInt(req.query.anyo);
    }

    if (req.query && req.query.tipo_examen) {
        match.tipo_examen = {
            $regex: new RegExp(req.query.tipo_examen, 'ig')
        };
    }

    match.mes = {};
    if (req.query && req.query.mes_a) {
        match.mes = _.extend(match.mes, {
            $gte: parseInt(req.query.mes_a)
        });
    }

    if (req.query && req.query.mes_b) {

        match.mes = _.extend(match.mes, {
            $lt: parseInt(req.query.mes_b)
        });
    }

    if (_.keys(match).length) {
        aggregate.push({$match: match});
    }

    aggregate.push(
        {
            $group: {
                _id       : {
                    codigo: "$codigo_autoescuela",
                    nombre: "$nombre_autoescuela"
                },
                conv_1    : {$sum: "$num_aptos_1conv"},
                conv_2    : {$sum: "$num_aptos_2conv"},
                conv_3o4  : {$sum: "$num_aptos_3o4conv"},
                conv_5omas: {$sum: "$num_aptos_5_o_mas_conv"},
                passed    : {$sum: {$add: ["$num_aptos_1conv", "$num_aptos_2conv"]}},
                failed    : {$sum: {$add: ["$num_aptos_3o4conv", "$num_aptos_5_o_mas_conv"]}},
                num_aptos : {$sum: "$num_aptos"},
                no_aptos  : {$sum: "$num_no_aptos"},
                count     : {$sum: 1},
            }
        }
    );

    aggregate.push(
        {
            $project: {
                conv_1     : 1,
                conv_2     : 1,
                conv_3o4   : 1,
                conv_5omas : 1,
                num_aptos  : 1,
                no_aptos   : 1,
                passed     : 1,
                failed     : 1,
                count      : 1,
                total      : {
                    $add: [
                        "$passed",
                        "$failed"
                    ]
                },
                passed_rate: {
                    $cond: [
                        {
                            $eq: ["$num_aptos", 0]
                        },
                        'NA',
                        {
                            $multiply: [
                                {
                                    $divide: ["$passed", "$num_aptos"]
                                },
                                100
                            ]
                        }
                    ]
                }
            }
        }
    );

    var query = Apto.aggregate(aggregate);
    query.sort({'passed_rate': -1});
    //query = queryBuild.queryCount(query, req.query, 200);
    query.exec(function (error, results) {
        if (req.query && req.query.min_aptos > 0) {
            results = _.filter(results, function (n) {
                if (n.num_aptos > req.query.min_aptos) return true;
            });
        }

        if (req.query && req.query.limit > 0) {
            results = results.slice(0, req.query.limit);
        }
        res.json(jsonRes.success({query: aggregate, params:req.query, field: 'aptos', response: results}));
    });

};

api = {
    all     : my.all,
    rankings: my.rankings
};

module.exports = api;
