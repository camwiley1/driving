var api = {},
    settings = {},
    _ = require('lodash');

api.queryString = function (query, params, fields) {
    _.forIn(fields, function (field, key) {
        var fieldLower = field.toLowerCase();
        if (params[fieldLower] && params[fieldLower].length) {
            var fieldPattern = params[fieldLower].replace(/,/, '|');
            var queryData = {};
            queryData[key] = new RegExp(fieldPattern);
            query.find(queryData);
        }
    });
    return query;
};

api.queryInt = function (query, params, fields) {
    _.forIn(fields, function (field, key) {
        if (typeof(params[field]) !== 'undefined' && params[field].length) {
            if (params[field].match(/\s$/)) {
                query.where(key).gt(params[field].replace(/\+/, ''));
            }
            else if (params[field].match(/\-$/)) {
                query.where(key).lt(params[field].replace(/\-/, ''));
            }
            else {
                console.log(params[field].split(','));
                query.where(key).in(params[field].split(','));
            }
        }
    });
    return query;
};

api.queryCount = function (query, params, fallback) {
    fallback = fallback >= 0 ? fallback : 100;
    if (parseInt(params.count) === -1) {
        return query;
    }
    var count = params.count && params.count > 0 ? params.count : fallback;
    count = parseInt(count, 10);
    if (count > 0) {
        query.limit(parseInt(count));
    }

    return query;
};

module.exports = api;


