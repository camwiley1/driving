var _ = require('lodash');

var format = {
    success: {ok: true},
    fail: {ok: false}
};

var json = {
    success: function (opts) {
        var response = opts.response || {};
        page = opts.page || 1;
        var field = opts.field || false;
        var res = _.merge({}, format.success);
        res.query = opts.query || '';
        res.params = opts.params || '';
        res.count = response.length || 0;
        res.page = page;
        var results = {};
        results[field] = response;
        return _.merge(res, results);
    }
};

api = {
    json: json
};


module.exports = api;
