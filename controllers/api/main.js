var Apto = require('../../models/apto'),
    router = require('express').Router(),
    methodsPath = './methods/',
    _ = require('lodash'),
    methods = {},
    methodNames = [
        'search',
        'escuelas',
        'centros'
    ];


var loadMethods = function (names) {
    _.forEach(names, function (n) {
        methods[n] = require(methodsPath + n);
    });
};

loadMethods(methodNames);

/**
 * Usage: /api/method.name
 */
router.get('/search.all', methods.search.all);

router.get('/search.rankings', methods.search.rankings);

router.get('/escuelas.list', methods.escuelas.search);

router.get('/centros.all', methods.centros.all);

router.get('/centros.list', methods.centros.search);

router.get('/escuelas.apto_conv', function (req, res, next) {
    var codigo = req.query && req.query.codigo;

    if (! codigo) {
        res.status(400).json({ok: false, error:'Bad request'});
        next()
    }
    Apto.find({codigo_autoescuela: codigo}, function (err, results) {
        if (err) {
            return next(err);
        }

        res.json({ok: true, rankings: results});
    });
});

module.exports = router;