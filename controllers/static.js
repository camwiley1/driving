var express = require('express');
var router = express.Router();
router.use(express.static(__dirname + '/../layouts'));
router.use(express.static(__dirname + '/../templates'));
router.use(express.static(__dirname + '/../public/css'));
router.use(express.static(__dirname + '/../public/js'));
router.use(express.static(__dirname + '/../bower_components'));
var options = {
    root: __dirname + '/../layouts/'
};

router.get('*', function (req, res) {
    res.sendFile('app.html', options);
});

module.exports = router;

