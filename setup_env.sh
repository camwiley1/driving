#!/usr/bin/env bash

# node install
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs iptables

echo "setting up node..."
npm install -g n && n stable
echo "done."

# express install
echo "setting up express..."
npm install -g express
echo "done."

# mongodb install
echo "setting up mongodb..."
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10 && echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list && sudo apt-get update && sudo apt-get install -y mongodb-org

# gulp install
echo "setting up gulp..."
npm install -g gulp
echo "done."

# remove :3000 in the URL
iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 3000
iptables --flush

echo "ALL DONE!"

