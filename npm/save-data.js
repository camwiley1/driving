var fs = require('fs');
var parse = require('csv-parse');
var Apto = require('../models/apto');
var db = require('../db');
var _ = require('lodash');
var expect = require('chai').expect;
var saveAutoescula = require('./save-autoescuela');
var saveCentro = require('./save-centro');
var q = require('q');
var filePath = './files/';

var clearDb = function (name) {
    db[name].drop();
    output(countDb(name));
};

var output = function (message) {
    console.log(message);
};

var countDb = function (name) {
    return (db[name]) ? db[name].count() : 0;
};

var verifyHeader = function (data) {
    expect(data).to.eql(fileColumnNames);
};

var getEnabledFields = function (header) {
    return _.filter(fileColumnNames, function (col) {
        return _.findIndex(header, function (val) {
            return val.toLowerCase() === col;
        }) >= 0;
    });
};

var exit = function (code) {
    code = code || 0;
    process.exit(code);
};

var fileColumnNames = [
    'desc_provincia',
    'centro_examen',
    'codigo_autoescuela',
    'nombre_autoescuela',
    'codigo_seccion',
    'mes',
    'anyo',
    'tipo_examen',
    'nombre_permiso',
    'num_aptos',
    'num_aptos_1conv',
    'num_aptos_2conv',
    'num_aptos_3o4conv',
    'num_aptos_5_o_mas_conv',
    'num_no_aptos'
];
var storeFile = function (src) {

    var readStream = fs.createReadStream(src);
    readStream.setEncoding('utf8');
    var allData = '';
    output('loading file:' + src);
    readStream.on('data', function (chunk) {
        allData += chunk.toString();
    })
        .on('error', function () {
            output('file does not exist');
            process.exit(1);
        })

        .on('end', function () {
            parse(allData, {delimiter: ';'}, function (err, res) {
                var header = res.shift();
                var enabledFields = getEnabledFields(header);
                storeData(res, enabledFields);
                //verifyHeader(header);
                //validateEntries(res).finally(function () {
                //storeData(res);
                //});
            })
        });
};

var generateUniqueId = function (row) {
    var b = new Buffer(_.map(row, function (n) {
        return n.replace(/ /g, '')
    }).join(''));
    return b.toString('base64');
};

var findUniqueId = function (uniqueId) {
    return Apto.findOne({uid: uniqueId});
};

var validateEntries = function (data) {
    var count = data.length;
    var firstEntry = data[0],
        firstUID = generateUniqueId(firstEntry);
    var lastEntry = data.slice(-1)[0],
        lastUID = generateUniqueId(lastEntry);

    var query = Apto.find();
    var promise1 = query.in('uid', [firstUID, lastUID]).exec();
    var promise2 = Apto.find({mes: firstEntry[5], anyo: firstEntry[6]}).count();

    var qAll = q.all([promise1, promise2]);
    qAll.done(function (values) {
        if (values[0].length === 2 && values[1] === count) {
            output('Data entries already found. Exiting...');
            process.exit(0);
        }
    });

    return qAll;
};

var padMonth = function (val) {
    var valStr = val + '';
    return (valStr.length < 2) ? '0' + valStr : valStr;
};

var getDate = function (year, month, day) {
    day = day || 1;
    return new Date(parseInt(year), parseInt(month) - 1, parseInt(day))
};

var setData = function (data, fields) {
    var result = {},
        index = 0;
    _.forEach(fields, function (n) {
        result[n] = data[index].trim();
        index++;
    });

    return result;
};

var storeData = function (data, enabledFields) {
    output("Total entries: " + data.length);
    var insertList = [],
        insertData;

    _.forEach(data, function (n) {
        insertData = setData(n, enabledFields);
        if (insertData['mes'] && insertData['anyo']) {
            insertData.date = getDate(insertData['anyo'], insertData['mes'], 1);
        }
        if (insertData['nombre_autoescuela']) {
            insertData['nombre_autoescuela'] = insertData['nombre_autoescuela'].replace(/(^ae\ |\ ae\ |^a\ e\ |^autoescuelas?\ |^autoescola\ |\ s\ l|\ sl$)/ig, '');
        }
        insertData.uid = generateUniqueId(n);
        insertList.push(insertData);
    });

    insertBatch(insertList);
};

var handleError = function (err) {
    // do nothing
};

var insertBatch = function (insertList) {
    var batchTotal = insertList.length;
    var insertCount = 0;
    var skipCount = 0;
    var start = Date.now();
    output(batchTotal);
    _.forEach(insertList, function (n) {
        var apto = new Apto(n);
        apto.save(function (err, value) {
            insertCount++;
            if (insertCount % 500 === 0) {
                output(insertCount);
            }
            if (err) {
                if (insertCount === batchTotal) {
                    insertDone(start);
                }
                return handleError(err);
            }

            if (apto.nombre_autoescuela && apto.nombre_autoescuela.length > 0) {
                saveAutoescula.add({
                    nombre: apto.nombre_autoescuela,
                    codigo: apto.codigo_autoescuela
                });
            }

            if (apto.centro_examen && apto.centro_examen.length > 0) {
                saveCentro.add({nombre: n.centro_examen, provincia: n.desc_provincia});
            }

            if (insertCount === batchTotal) {
                insertDone(start);
            }
        });
    });
};

var insertDone = function (start) {
    var elapsedSecs = (Date.now() - start) / 1000;
    output('Done!!!!');
    output('Execution time: ' + elapsedSecs);
    process.exit(0);
};

var findDuplicates = function (insertData) {
    var duplicateList = [];
    var totalCount = 0;
    var start = Date.now();
    var insertList = [];
    var batchTotal = length;

    var insertCount = 0;
    Apto.findOne({uid: insertData.uid}, function (err, res) {
        if (err) return handleError(err);

        totalCount++;
        // Only insert for unique entries
        if (!res) {
            insertList.push(insertData);
        } else {
            duplicateList.push(insertData);
        }

        if (totalCount === length) {
            output('Duplicates Total: ' + duplicateList.length);
            output('Insert Total: ' + insertList.length);
            var elapsedSecs = (Date.now() - start) / 1000;
            output('Execution time(sec): ' + elapsedSecs);

            if (insertList.length > 0) {
                insertBatch(insertList);
            }
            else {
                process.exit(0);
            }
        }
    });
};

var processArguments = function () {
    var args = process.argv.slice(2);

    if (args.length < 1) {
        output('file_name is required');
    }
    var fileName = args[0];
        //fullPath = filePath + fileName;

    storeFile(fileName);
};

processArguments();

//storeFile('./files/ABRIL.txt');
