var CentroExamen = require('../models/centro-examen'),
    _ = require('lodash'),
    api,
    addCount = 0;

var add = function (data) {
    data = filter(data);
    var ce = new CentroExamen(data);
    return ce.save();
};

var addBatch = function (collection) {

};

var exists = function (data) {
    return CentroExamen.findOne({nombre: data.nombre, provincia: data.provincia});
};

var filter = function (data) {
    data.nombre = data.nombre.replace(/\([^)]+\)/i, '');
    data.nombre = data.nombre.trim();
    return data;
};

api = {
    add: add,
    filter: filter
};

module.exports = api;