var Autoescuela = require('../models/autoescuela'),
    _ = require('lodash'),
    api;

var add = function (data) {
    data = filterName(data);
    var ae = new Autoescuela(data);
    ae.save(function (err, value) {
        if (err) {
            return (err);
        }
    });
};

var filterName = function (data) {
    //data.nombre = data.nombre.trim();
    //data.nombre = data.nombre.replace(/(^ae\ |^a\ e\ |^autoescuelas?\ |^autoescola\ |s\ l$)/, '');
    return data;
};

var updateData = function (data) {
    data = filterName(data);
    Autoescuela.where({_id: data._id}).update({nombre: data.nombre}, function(err, res) {
        if (err) {
            process.exit(1);
        }
    });
};

var filterAll = function () {
    Autoescuela.find().exec(function (err, results) {
        _.forEach(results, function (n, key) {
            updateData(n);
        });
    });
};

api = {
    add: add,
    filter: filterAll
};

module.exports = api;