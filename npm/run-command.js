var exec = require('child_process').exec;

var callback = function (error, stdout, stderr) {
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);

    if (error !== null) {
        console.log('exec error: ' + error);
    }
};

var runCommand = function (command) {
    exec(command, callback);
};


module.exports =  {
    run: runCommand
};