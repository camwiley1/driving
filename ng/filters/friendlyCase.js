angular.module('drivingApp').filter('friendlyCase', function (capitalizeFilter) {
    return function (input) {
        input = input.replace('_', ' ');
        return capitalizeFilter(input);
    }
});
