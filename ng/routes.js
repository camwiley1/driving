angular.module('drivingApp')
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'SearchCtrl',
                templateUrl: 'search.html'
            })
            .when('/search', {
                controller: 'SearchCtrl',
                templateUrl: 'search.html'
            })
            .when('/search.all', {
                controller: 'AptosCtrl',
                templateUrl: 'search-all.html'
            })
            .when('/search.rankings', {
                controller : 'AptosSumCtrl',
                templateUrl: 'aptos-sum.html'
            })
            .when('/search.centros', {
                controller : 'CentrosCtrl',
                templateUrl: 'centros.html'
            })
            .when('/search.provincias', {
                controller : 'CentrosCtrl',
                templateUrl: 'provincias.html'
            })
            .when('/escuelas', {
                controller : 'EscuelasCtrl',
                templateUrl: 'escuelas.html'
            })
            .when('/rankings', {
                controller : 'RankingsCtrl',
                templateUrl: 'rankings.html'
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });
