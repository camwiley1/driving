
angular.module('drivingApp').controller('AptosCtrl', function ($scope, myUtils, AptosSrv, _) {
    AptosSrv.fetch().then(function (posts) {
        var rawPosts = myUtils.promiseDataField(posts, 'aptos');
        var preppedPosts = _.map(rawPosts, function (n) {
            n.date = new Date(n.anyo, n.mes, 1);
            return n;
        });
        $scope.aptos = preppedPosts;
    });
});
