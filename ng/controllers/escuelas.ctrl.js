
angular.module('drivingApp').controller('EscuelasCtrl', function ($scope, myUtils, EscuelasSrv) {
    EscuelasSrv.fetch().then(function (results) {
        $scope.escuelas = myUtils.promiseDataField(results, 'escuelas');
        $scope.count = myUtils.promiseDataField(results, 'count');
    });
});
