angular.module('drivingApp').controller('RankingsCtrl', function ($scope, myUtils, AptosSrv, _, $routeParams, $route) {
    var opts = {
        min_aptos     : 20, // minimum number of applications
        limit         : 100,
        mes_b         : 7,
        anyo          : 2015,
        tipo_examen   : 'Conducción Y Circulación',
        nombre_permiso: 'b' // max number of results
    };

    var translateParams = {
        mes_a: 'De Mes',
        mes_b: 'A Mes',
        min_aptos: 'Minimo Aptos',
        centro_examen: 'Centro de Examen',
        limit: 'Limite de Resultados',
        anyo: 'Año',
        provincia: 'Provincia',
        tipo_examen: 'Tipo Examen',
        nombre_permiso: 'Nombre Permiso'
    };

    function rankingsSuccess(posts) {
        var rawPosts = myUtils.promiseDataField(posts, 'aptos');
        var count = 1;
        var preppedPosts = _.map(rawPosts, function (n) {
            n.rank = count;
            n.date = new Date(n.anyo, n.mes, 1);
            count++;
            return n;
        });
        $scope.aptos = preppedPosts;
        $scope.params = myUtils.promiseDataField(posts, 'params');

        var translatedParams = {};
        _.forEach($scope.params, function (n, key) {
            translatedParams[translateParams[key] || key] = n;
        });
        $scope.niceParams = translatedParams;
    }

    $scope.toggleFilterInputs = function () {
        $scope.showFilterInputs = ($scope.showFilterInputs) ? false : true;
    };

    var data = _.extend({}, $routeParams, opts);
    AptosSrv.fetchRankings(opts).then(rankingsSuccess);
    $scope.data = data;

    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.submitSearch = function () {
        AptosSrv.fetchRankings($scope.data).then(rankingsSuccess);
        $scope.toggleFilterInputs();
    };
});
