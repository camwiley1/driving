
angular.module('drivingApp').controller('CentrosCtrl', function ($scope, myUtils, CentrosSvc, _) {
    CentrosSvc.fetchAll().then(function (results) {
        var centros = myUtils.promiseDataField(results, 'centros');
        $scope.centros = centros;
        $scope.provincias = _.unique(_.pluck(centros, 'provincia').sort());
    });
});
