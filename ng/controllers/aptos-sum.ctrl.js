
angular.module('drivingApp').controller('AptosSumCtrl', function ($scope, myUtils, AptosSrv, _) {
    AptosSrv.fetchSum().then(function (posts) {
        var rawPosts = myUtils.promiseDataField(posts, 'aptos');
        var count = 1;
        var preppedPosts = _.map(rawPosts, function (n) {
            n.rank = count;
            n.date = new Date(n.anyo, n.mes, 1);
            count++;
            return n;
        });
        $scope.aptos = preppedPosts;
    });
});
