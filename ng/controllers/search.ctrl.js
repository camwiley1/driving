angular.module('drivingApp').controller('SearchCtrl', function ($scope, $location, myUtils, CentrosSvc, _) {

    const PROVINCIA = 'provincia';
    const CENTRO = 'centro_examen';

    var my = {},
        cache = {};

    my.validateSelected = function (name) {
        return !!($scope.data && $scope.data[name])
    };

    my.validateAllSelected = function () {
        return my.validateSelected(PROVINCIA) && my.validateSelected(CENTRO);
    };

    my.setData = function (name, value) {
        if ($scope.data && $scope.data[name]) {
            $scope.data[name] = value;
        }
    };

    my.getData = function (name) {
        return $scope.data && $scope.data[name] || '';
    };

    my.clearData = function (name) {
        my.setData(name, '');
    };

    $scope.selectCentro = function () {
        $scope.searchEnabled = true;
    };

    $scope.searchProvincia = function () {
        $scope.centroEnabled = false;
        var value = my.getData(PROVINCIA);
        if (value.length < 1) {
            $scope.provinciaMatches = [];
            $scope.searchProvinciaEnabled = false;
            $scope.searchEnabled = false;
            my.clearData(CENTRO);
            return;
        }

        $scope.searchProvinciaEnabled = true;

        var regex = new RegExp('^' + value);
        var matches = _.filter($scope.provincias, function (n) {
            return regex.test(n);
        });

        $scope.provinciaMatches = matches;
    };

    $scope.selectProvincia = function (provincia) {
        if (! provincia) {
            provincia = ($scope.provinciaMatches.length === 1)  ? $scope.provinciaMatches[0] : false;
            if (! provincia) return;
        }
        my.setData(PROVINCIA, provincia);
        $scope.provinciaMatches = [];
        $scope.searchProvinciaEnabled = false;
        $scope.centroEnabled = true;
        $scope.searchCentros();
    };

    $scope.searchCentros = function () {
        $scope.centroEnabled = my.validateSelected('provincia');
        if ($scope.centroEnabled) {
            var match = _.filter(cache.centros, function (n) {
                return n.provincia === $scope.data.provincia;
            });
            $scope.centros = [];
            if (match.length) {
                $scope.centros = _.uniq(_.pluck(match, 'nombre')).sort();
                $scope.data.centro_examen = '';
                if ($scope.centros.length === 1) {
                    $scope.data.centro_examen = $scope.centros[0];
                }
            }
        }
    };

    $scope.submitSearch = function () {
        if (my.validateAllSelected()) {
            $location.path('/rankings').search($scope.data);
        }
    };

    var fetchAllPromise = CentrosSvc.fetchAll();
    fetchAllPromise.then(function (results) {
        cache.centros = myUtils.promiseDataField(results, 'centros');
        $scope.provincias = _.uniq(_.pluck(cache.centros, PROVINCIA)).sort();
    });

}).directive('searchPage', function ($timeout) {
    var resetSelected = function ($elems) {
        $elems.removeClass('selected');
    };

    var selectOne = function ($elem) {
        $elem.addClass('selected');
    };

    return {
        link    : function ($scope, $elem, $attrs) {
            var selectors = {
                'searchProvincia' : '#search-provincia'
            };
            $timeout(function () {
                var selector = '#results-centros li';
                $elem.on('click', selector, function () {
                    $scope.data['centro_examen'] = $(this).text();
                    resetSelected($(selector));
                    selectOne($(this));
                });

                $scope.$watch('centroEnabled', function (newValue, oldValue) {
                    if (newValue) {
                        $(selectors.searchProvincia).blur();
                    }
                });
            })
        }
    };
});
