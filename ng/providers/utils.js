angular.module('drivingApp').factory('myUtils', function () {

    var api = {};
    api.successMessage = function (message) {
        console.log('"' + message + '" => success :)');
    };

    api.errorMessage = function (message) {
        throw new Error('Wooops: There was an error with the "' + message + '" :(');
    };

    api.promiseDataField = function (response, field, fallback) {
        return api.valueOrFallback(response && response.data && response.data[field], fallback);
    };

    api.valueOrFallback = function (value, fallback) {
        return typeof value === 'undefined' ? fallback : value;
    };

    api.params = function (data) {
        return Object.keys(data).map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
        }).join('&');
    };

    return api;

});
