
angular.module('drivingApp', [
    'ngRoute'
])
    .constant('_', window._)
    .run(function ($rootScope, $timeout) {
        $rootScope._ = window._;
        $rootScope.$on('$viewContentLoaded', ()=> {
            $timeout(()=> {
                componentHandler.upgradeAllRegistered();
        });
    })

    });
