
angular.module('drivingApp').service('CentrosSvc', function ($http, $routeParams) {
    var api = {};

    api.fetch = function () {
        return $http.get('/api/centros.search', {
            params: $routeParams
        });
    };

    api.fetchAll = function () {
        return $http.get('/api/centros.all');
    };

    return api;
});


