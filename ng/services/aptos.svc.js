angular.module('drivingApp').service('AptosSrv', function ($http, $routeParams, _) {
    this.fetch = function () {
        return $http.get('/api/search.all', {
            params: $routeParams
        });
    };

    this.fetchSum = function () {
        return $http.get('/api/search.rankings', {
            params: $routeParams
        });
    };

    this.fetchRankings = function (opts) {
        // inclusion criteria
        return $http.get('/api/search.rankings', {
            params: _.extend($routeParams, opts)
        });
    };
});


