
angular.module('drivingApp').service('EscuelasSrv', function ($http, $routeParams) {
    this.fetch = function () {
        return $http.get('/api/escuelas.list', {
            params: $routeParams
        });
    };

});


